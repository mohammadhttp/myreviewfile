'use strict';
const path = require('path');
const webpack=require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin'); //installed via npm
const js_address_sort=require('./template/webpack_plugin/js_address_sort/js_address_sort');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const autoprefixer = require("autoprefixer");
/*const regeneratorRuntime = require("regenerator-runtime");*/
const copyWebpackPlugin = require('copy-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

let pathsToClean = [
    'dist'
];

let cleanOptions = {
    root:     __dirname,
    exclude:  [],
    verbose:  true,
    dry:      false
};


module.exports = {
    /*mode: "production",*/
    mode: "development",
    entry: {
        style:'./template/js/public/style.js',

        /*quill:'./template/js/notPublic/quill/style.js',*/
        tinymce:'./template/js/notPublic/tinymce/style.js',
        leaflet:'./template/js/notPublic/leaflet/style.js',
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname,'template/dist')
    },
    resolve: {
        alias: {
            'appPath': path.resolve(__dirname, 'template/js/public/appPath.js'),
            'vue$': 'vue/dist/vue.js',
        },
        extensions: ['*','.js','.jsx', '.ts','.vue', '.svg']
    },
    externals:{
        'tinymce': path.resolve(__dirname, 'node_modules/tinymce/tinymce.js')
    },
    performance: { hints: false },
    /*devtool: 'source-map',*/
    module: {

        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            /*{
                test: /\.ts$/,
                use: [{
                    loader: 'ts-loader',
                    options: {
                        compilerOptions: {
                            declaration: false,
                            target: 'es5',
                            module: 'commonjs'
                        },
                        transpileOnly: true
                    }
                }]
            },
            {
                test: /quill[^/].+\.svg$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        minimize: true
                    }
                }]
            },*/








            /* BABEL-LOADER */
            {
                test: /\.(m?js|jsx)$/,
                /*exclude: /(node_modules|bower_components)/,*/
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env','@babel/preset-react']
                    }
                }
            },







            /* CSS-LOADER OR SASS-LOADER*/
            {
                test: /\.(sa|sc|c)ss$/,

                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '../',
                            hmr: process.env.NODE_ENV === 'development',
                            /*reloadAll: true*/
                        }
                    },
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        /*options: {
                            sourceMap: true,
                            config: {
                                path: 'postcss.config.js'
                            }
                        }*/
                        options: {
                            plugins: () => [autoprefixer()]
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            webpackImporter: false,
                            sassOptions: {
                                includePaths: ['./node_modules']
                            },
                            // Prefer Dart Sass
                            implementation: require('dart-sass'),
                        }
                    }
                ],
            },






            /* FILE-LOADER */
            {
                test: /\.(eot|ttf|woff|woff2)$/,
                exclude: /(quill)/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath:'font',
                            name: '[name].[ext]',
                        },
                    },
                ],
            },
            {
                test: /\.(svg|png|jpe?g|gif)$/,
                exclude: /(quill)/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            outputPath:'images',
                            name: '[name].[ext]',
                        },
                    },
                ],
            },




            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },



            /* JSHINT-PRELOADER */
            {
                test: /.js$/,
                enforce: 'pre',
                exclude: /node_modules|vue/,
                use: [
                    {
                        loader: `jshint-loader`,
                        options: {
                            /*camelcase: true,
                            emitErrors: false,
                            failOnHint: false,*/
                            esversion: 6
                        }
                    }
                ]
            },

        ]
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        /*new CleanWebpackPlugin(),*/
        new js_address_sort(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            filename: 'css/[name].css',
            ignoreOrder: false,

        }),
        new copyWebpackPlugin([
            { from: './template/js/notPublic/tinymce/langs', to: './langs' }/*,
            { from: './node_modules/tinymce/themes', to: './themes' },
            { from: './node_modules/tinymce/skins', to: './skins' }*/
        ]),
        /*new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer()
                ]
            }
        }),*/
        new VueLoaderPlugin(),
    ]
};